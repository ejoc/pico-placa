
// const PLATE_NUMBER_REGEX = /^()/$i;

const picoPlacaRules = [
  {day: 1, restrict: [1, 2]},
  {day: 2, restrict: [3, 4]},
  {day: 3, restrict: [5, 6]},
  {day: 4, restrict: [7, 8]},
  {day: 5, restrict: [9, 0]},
]

const timeRules = [
  [[7, 0, 0], [9, 30, 0]],
  [[16, 0, 0], [19, 30, 0]]
]

function compareRangeTimes(date) {
  const timeRange = timeRules.map((hourRange) => {
    let startDate = new Date(date.getTime())
    startDate = new Date(startDate.setHours(...hourRange[0]))
    let endDate = new Date(date.getTime())
    endDate = new Date(endDate.setHours(...hourRange[1]))
    return [startDate, endDate]
  })
  return timeRange.some(range => date >= range[0] && date <= range[1])
}

function checkPicoPlaca(date, plateNumber) {
  const day = date.getDay()
  const rule = picoPlacaRules.find(rule => rule.day === day)
  return rule.restrict.includes(plateNumber) && compareRangeTimes(date)
}

function calculePicoPlaca(plateNumber, date, time) {
  // validate params

  const dateTime = new Date(`${date} ${time}`)
  const lastNumber = Number(plateNumber.slice(-1))

  return checkPicoPlaca(dateTime, lastNumber)
}

module.exports = calculePicoPlaca