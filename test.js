const calculePicoPlaca = require('./index')

test('pico & placa test', () => {
  expect(calculePicoPlaca("ACD-2001", "2019-05-06", "09:31:00")).toBe(false)

  expect(calculePicoPlaca("ACD-2001", "2019-05-06", "09:30:00")).toBe(true) // multa
})